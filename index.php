<?php
// Recupera a URL da requisição e seus parâmetros, separando em um vetor dividido pelo caracter "/"
$url = explode( "/", str_replace( strrchr( $_SERVER["REQUEST_URI"], "?" ), "", $_SERVER["REQUEST_URI"] ) );
array_shift( $url );

// Considera o primeiro parâmetro como o arquivo .php

$tipo 	= $url[1];
$params = $url[2] ?? '';

if ($tipo == '') {
	//Rota home /
	header('Location: home');
}
elseif (is_file($tipo . '.php')) {
	//Se a rota for um arquivo php
	if ($params!="") {
		$_REQUEST["url"] = $params;
		$_REQUEST["route"] = "1";
	}
    include($tipo . '.php');
}
else {
	//página não encontrada
	include("404.php");
}
?>