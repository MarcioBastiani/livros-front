<!DOCTYPE html>
<html>
<head>
    <!--Import Google Icon Font-->
    <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
    <!--Import materialize.css-->
    <link type="text/css" rel="stylesheet" href="css/materialize.min.css"  media="screen,projection"/>

    <!--Let browser know website is optimized for mobile-->
    <meta name="viewport" content="width=device-width, initial-scale=1.0"/>
</head>
<body>
    <header>
        <nav>
            <div class="nav-wrapper container">
                <a href="#!" class="brand-logo">Minha Biblioteca</a>
                <a href="#" data-target="mobile-demo" class="sidenav-trigger"><i class="material-icons">menu</i></a>
                <ul class="right hide-on-med-and-down">
                    <li><a href="home">Livros</a></li>
                    <li><a href="autores">Autores</a></li>
                </ul>
            </div>
        </nav>

        <ul class="sidenav" id="mobile-demo">
            <li><a href="home">Livros</a></li>
            <li><a href="autores">Autores</a></li>
        </ul>
    </header>

    <div class="container">
        <div class="row">
            <div class="col s12 m8 offset-m2">
                <h3 class="light">Meus Livros</h3>

                <div class="progress hide" id="progress">
                    <div class="indeterminate"></div>
                </div>
                
                <ul class="collection" id="livros">
                    <li class="collection-item avatar">
                        <img src="images/ograndecomputador.jpg" alt="" class="circle">
                        <span class="title">O Grande Computador Celeste</span>
                        <p>Marcelo Del Debbio</p>
                        <small>650 páginas</small>
                        <a href="#modalLivro" class="secondary-content modal-trigger"><i class="material-icons">edit</i></a>
                    </li>
                    <li class="collection-item avatar">
                        <img src="images/ocaibalion.jpg" alt="" class="circle">
                        <span class="title">O Caibalion</span>
                        <p>Três Iniciados</p>
                        <small>500 páginas</small>
                        <a href="#" class="secondary-content"><i class="material-icons">edit</i></a>
                    </li>
                    <li class="collection-item avatar">
                        <img src="images/jquery.jpg" alt="" class="circle">
                        <span class="title">jQuery. A Biblioteca do Programador Javascript</span>
                        <p>Maurício Samy Silva</p>
                        <small>544 páginas</small>
                        <a href="#" class="secondary-content"><i class="material-icons">edit</i></a>
                    </li>
                </ul>

                <ul class="pagination">
                    <li class="disabled"><a href="#!"><i class="material-icons">chevron_left</i></a></li>
                    <li class="active"><a href="#!">1</a></li>
                    <li class="waves-effect"><a href="#!">2</a></li>
                    <li class="waves-effect"><a href="#!">3</a></li>
                    <li class="waves-effect"><a href="#!">4</a></li>
                    <li class="waves-effect"><a href="#!">5</a></li>
                    <li class="waves-effect"><a href="#!"><i class="material-icons">chevron_right</i></a></li>
                </ul>

            </div>
        </div>

        <table id="livros" class="highlight hide">
            <thead>
                <tr>
                    <th>Título</th>
                    <th>Páginas</th>
                    <th class="center-align">Lido</th>
                </tr>
            </thead>

            <tbody></tbody>
        </table>
    </div>
    
    <!-- Modal Edição -->
    <div id="modalLivro" class="modal">
        <div class="modal-content">
            <h4>O Grande Computador Celeste</h4>
            <h5>Marcelo del Debbio</h5>
            <p>650 páginas</p>
        </div>
        <div class="modal-footer">
            <a href="#" class="modal-close waves-effect waves-green btn-flat">Fechar</a>
        </div>
    </div>

    <!--JavaScript at end of body for optimized loading-->
    <script type="text/javascript" src="js/jquery-3.4.1.min.js"></script>
    <script type="text/javascript" src="js/materialize.min.js"></script>
    <script>
        var base_url = 'http://localhost:8050/livros/public';

        const view_livros = {
            refresh: function() {
                $("#progress").removeClass("hide");

                $.get(base_url + '/livros', {'nome': 'o'}, function(data, status, xhr) {
                    if (data.erro) {
                        console.log(data.erro);
                    }
                    else {
                        $("#livros").removeClass("hide");
                        $("#livros tbody").empty();

                        $.each(data, function(index,livro) {                        
                            var tr = '';
                            tr+= '<tr>';
                            tr+= '  <td>' + livro.nome + '</td>';
                            tr+= '  <td>' + livro.paginas + '</td>';
                            tr+= '  <td class="center-align">' + livro.lido + '</td>';
                            tr+= '</tr>';
                            
                            $("#livros tbody").append(tr);
                        });
                    }
                })
                .fail( function(xhr, status, error) {
                    alert("Erro ao acessar o endpoint: " + base_url + '/livros');
                })
                .always( function() {
                    $("#progress").addClass("hide");
                });
            }
        };

        $(document).ready(function(){
            $('.sidenav').sidenav();
            $('.modal').modal();

            //view_livros.refresh();
        });
    </script>
</body>
</html>